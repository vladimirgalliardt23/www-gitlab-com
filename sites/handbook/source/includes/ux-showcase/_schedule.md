[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                        | Presenter 1     | Presenter 2     | Presenter 3      | 
| ---------- | --------------------------- | --------------- | --------------- | ---------------- | 
| 2022-08-03 | Justin Mandell              | Matt Nearents   | Amelia Bauerly  | Daniel Mora      | 
| 2022-08-17 | Taurie Davis                | Alexis Ginsberg | Emily Sybrant   | Matej Latin      | 
| 2022-08-31 | Marcel van Remmerden (APAC) | Michael Le      | Marcin Sędłak-Jakubowski | Evan Read        | 
| 2022-09-14 | Marcel van Remmerden        | Nick Brandt     | Emily Bauman    | Nick Leonard     | 
| 2022-09-28 | Rayana Verissimo            | Sunjung Park    | Michael Fangman | Camellia X Yang  | 
| 2022-10-12 | Blair  Christopher          | Matt Nearents   | Alexis Ginsberg | Philip Joyce     | 
| 2022-10-26 | Jacki  Bauerly              | Becka Lippert   | Libor Vanc      | Andy Volpe       | 
| 2022-11-09 | Justin Mandell              | Matej Latin     | Kevin Comoli    | Ali Ndlovu       | 
| 2022-11-23 | Taurie Davis                |                 |                 |                  | 
| 2022-12-07 | TBD (APAC)                  | Katie Macoy     |                 |                  | 
| 2022-12-21 | Marcel van Remmerden        |                 |                 |                  | 
